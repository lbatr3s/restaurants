//
//  RestaurantTableViewCell.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/25/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var sortingLabel: UILabel!
    @IBOutlet weak var favoriteImageView: UIImageView!
    
    static let nibName = String(describing: RestaurantTableViewCell.self)
    static let reuseIdentifier = "restaurantTableViewCell"
    
    func configure(_ restaurant: Restaurant, sortingBy sort: Sorting) {
        nameLabel.text = restaurant.name
        stateLabel.text = restaurant.status.rawValue
        
        if restaurant.isFavorite {
            favoriteImageView.image = #imageLiteral(resourceName: "favorite")
        } else {
            favoriteImageView.image = nil
        }
        
        if sort != .none {
            sortingLabel.text = String(describing: restaurant.selectedSort(sortValue: sort))
        }
    }
}
