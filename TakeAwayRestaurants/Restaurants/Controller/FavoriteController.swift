//
//  FavoriteController.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/26/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation

class FavoriteController {
    
    private let filename = "favoriteRestaurants"
    private let filemanager = FileManager.default
    private var restaurants: [Restaurant]!
    
    static let sharedInstance = FavoriteController()
    
    private init() {
        if let storedRestaurants = unarchiveObjectFromFile(file: filename, as: [Restaurant].self)  {
            restaurants = storedRestaurants
        } else {
            restaurants = []
        }
    }
    
    func saveRestaurant(_ restaurant: Restaurant) {
        var restaurant = restaurant
        restaurant.markAsFavorite()
        restaurants.append(restaurant)
        archiveObjectToFile(restaurants, file: filename)
    }
    
    func removeRestaurant(_ restaurant: Restaurant) {
        guard let index = restaurants.index(where: {(anotherRestaurant) -> Bool in
            return restaurant.name == anotherRestaurant.name
        }) else {
            return
        }
        
        var restaurant = restaurant
        restaurant.unfavorite()
        
        restaurants.remove(at: index)
    }
    
    func fetchRestaurants(sortedBy sortOption: Sorting = .none) -> [Restaurant] {
        var sortedRestaurants: [Restaurant]
        
        switch sortOption {
        case .bestMatch, .ratingAverage, .popularity:
            sortedRestaurants = restaurants.sorted(by: { $0.selectedSort(sortValue: sortOption) > $1.selectedSort(sortValue: sortOption) })
        case .newest, .distance, .averageProductPrice, .deliveryCosts, .minCost:
            sortedRestaurants = restaurants.sorted(by: { $0.selectedSort(sortValue: sortOption) < $1.selectedSort(sortValue: sortOption) })
        case .none:
            sortedRestaurants = restaurants.sorted(by: { $0.status.sortingOrder < $1.status.sortingOrder })
        }
        
        return sortedRestaurants
    }
    
    
    // MARK: Private methods
    
    private func archiveObjectToFile<T: Encodable>(_ object: T, file: String, atDirectory directory: String? = nil) {
        let filePath = createPathForFile(file: file, atDirectory: directory)
        
        let encoder = JSONEncoder()
        
        do {
            let data = try encoder.encode(object)
            
            if filemanager.fileExists(atPath: filePath) {
                try filemanager.removeItem(atPath: filePath)
            }
            
            filemanager.createFile(atPath: filePath, contents: data)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func unarchiveObjectFromFile<T: Decodable>(file: String, atDirectory directory: String? = nil, as type: T.Type) -> T? {
        let filePath = createPathForFile(file: file, atDirectory: directory)
        
        if !filemanager.fileExists(atPath: filePath) {
            return nil
        }
        
        if let data = filemanager.contents(atPath: filePath) {
            let decoder = JSONDecoder()
            
            do {
                let object = try decoder.decode(type, from: data)
                return object
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
    
    private func removeFileAtPath(path: String, atDirectory directory: String? = nil) {
        let filePath = createPathForFile(file: path, atDirectory: directory)
        
        do {
            try filemanager.removeItem(atPath: filePath)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        guard let documentsDirectory = paths.first else {
            abort()
        }
        
        return documentsDirectory
    }
    
    private func createPathForFile(file: String, atDirectory directory: String? = nil) -> String {
        var filePath: String
        
        if let customDirectory = directory {
            filePath = customDirectory.appending(file)
        } else {
            filePath = getDocumentsDirectory().appending(file)
        }
        
        return filePath
    }
}
