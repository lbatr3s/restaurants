//
//  RestaurantController.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/25/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

protocol RestaurantControllerDelegate: class {
    
    func didFetchRestaurants(_ restaurants: [Restaurant], favorites: [Restaurant])
    func didFailFetchingRestaurants()
}

class RestaurantController {
    
    private let dataController = RestaurantDataController()
    
    weak var delegate: RestaurantControllerDelegate?
    
    func fetchRestaurants(sortedBy sortOption: Sorting = .none) {
        if let restaurants = dataController.fetchRestaurants(sortedBy: sortOption) {
            let favorites = updateFavoriteFlag(favorites: FavoriteController.sharedInstance.fetchRestaurants(sortedBy: sortOption))
            
            delegate?.didFetchRestaurants(restaurants, favorites: favorites)
        } else {
            delegate?.didFailFetchingRestaurants()
        }
    }
    
    
    // MARK: Private methods
    
    private func updateFavoriteFlag(favorites: [Restaurant]) -> [Restaurant] {
        var updatedFavorites: [Restaurant] = []
        
        for var favorite in favorites {
            favorite.markAsFavorite()
            updatedFavorites.append(favorite)
        }
        
        return updatedFavorites
    }
}
