//
//  RestaurantDataController.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/19/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import Foundation

class RestaurantDataController {
    
    func fetchRestaurants(sortedBy sortOption: Sorting = .none) -> [Restaurant]? {
        guard let restaurants = parseRestaurants() else {
            return nil
        }
        
        var sortedRestaurants: [Restaurant]
        
        switch sortOption {
        case .bestMatch, .ratingAverage, .popularity:
            sortedRestaurants = restaurants.sorted(by: { $0.selectedSort(sortValue: sortOption) > $1.selectedSort(sortValue: sortOption) })
        case .newest, .distance, .averageProductPrice, .deliveryCosts, .minCost:
            sortedRestaurants = restaurants.sorted(by: { $0.selectedSort(sortValue: sortOption) < $1.selectedSort(sortValue: sortOption) })
        case .none:
            sortedRestaurants = restaurants
        }
        
        sortedRestaurants = removeFavorites(restaurants: sortedRestaurants)
        
        return sortedRestaurants
    }
    
    // MARK: Private methods
    
    private func loadJson() -> Data? {
        do {
            guard let file = Bundle.main.url(forResource: "restaurants", withExtension: "json") else {
                return nil
            }
            
            let data = try Data(contentsOf: file)
            
            return data
        } catch {
            return nil
        }
    }
    
    private func parseRestaurants() -> [Restaurant]? {
        guard let jsonData = loadJson() else {
            return nil
        }
        
        do {
            let decoder = JSONDecoder()
            let restaurantList = try decoder.decode(RestaurantsList.self, from: jsonData)
            let restaurants = restaurantList.restaurants
            let orderedRestaurants = restaurants.sorted(by: { $0.status.sortingOrder < $1.status.sortingOrder })
            
            return orderedRestaurants
        } catch {
            return nil
        }
    }
    
    private func removeFavorites( restaurants: [Restaurant]) -> [Restaurant] {
        var restaurants = restaurants
        let favorites = FavoriteController.sharedInstance.fetchRestaurants()
        
        for favorite in favorites {
            if let index = restaurants.index(where: {$0.name == favorite.name} ) {
                restaurants.remove(at: index)
            }
        }
        
        return restaurants
    }
}
