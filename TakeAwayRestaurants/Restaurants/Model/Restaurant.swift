//
//  Restaurant.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/19/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

struct Restaurant: Codable {
    
    let name: String
    let status: Status
    let sortingValues: Sort
    
    var isFavorite = false
    
    enum CodingKeys: String, CodingKey {
        case name
        case status
        case sortingValues
    }
    
    func selectedSort(sortValue: Sorting) -> Double {
        switch sortValue {
        case .bestMatch:
            return self.sortingValues.bestMatch
        case .newest:
            return self.sortingValues.newest
        case .ratingAverage:
            return self.sortingValues.ratingAverage
        case .distance:
            return Double(self.sortingValues.distance)
        case .popularity:
            return self.sortingValues.popularity
        case .averageProductPrice:
            return Double(self.sortingValues.averageProductPrice)
        case .deliveryCosts:
            return Double(self.sortingValues.deliveryCosts)
        case .minCost:
            return Double(self.sortingValues.minCost)
        case .none:
            return 0
        }
    }
    
    mutating func markAsFavorite() {
        isFavorite = true
    }
    
    mutating func unfavorite() {
        isFavorite = false
    }
}

enum Status: String, Codable {
    case open
    case closed
    case orderAhead = "order ahead"
    
    var sortingOrder: Int {
        switch self {
        case .open:
            return 0
        case .orderAhead:
            return 1
        case .closed:
            return 2
        }
    }
    
    init(initialValue: String) {
        self = Status(rawValue: initialValue) ?? .closed
    }
}
