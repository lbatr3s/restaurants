//
//  RestaurantsList.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/25/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

struct RestaurantsList: Codable {
    
    let restaurants: [Restaurant]
}
