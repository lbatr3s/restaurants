//
//  Sort.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/19/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

enum Sorting: CustomStringConvertible {
    
    case bestMatch
    case newest
    case ratingAverage
    case distance
    case popularity
    case averageProductPrice
    case deliveryCosts
    case minCost
    case none
    
    var description: String {
        switch self {
        case .bestMatch:
            return "Best Match"
        case .newest:
            return "Newest"
        case .ratingAverage:
            return "Rating Average"
        case .distance:
            return "Distance"
        case .popularity:
            return "Popularity"
        case .averageProductPrice:
            return "Average Product Price"
        case .deliveryCosts:
            return "Delivery Costs"
        case .minCost:
            return "Min Cost"
        case .none:
            return "None"
        }
    }
}

struct Sort: Codable {
    
    let bestMatch: Double
    let newest: Double
    let ratingAverage: Double
    let distance: Int
    let popularity: Double
    let averageProductPrice: Int
    let deliveryCosts: Int
    let minCost: Int
}
