//
//  RestaurantsTableViewController.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/19/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

class RestaurantsTableViewController: UITableViewController {
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var restaurants: [Restaurant] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var favorites: [Restaurant] = []
    private var sortingOption: Sorting = .none
    private var filteredRestaurants: [Restaurant] = []
    
    private var hasFavorites: Bool {
        return favorites.count > 0
    }
    
    lazy var restaurantController: RestaurantController = {
        return RestaurantController()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        restaurantController.delegate = self
        configureTableView(tableView)
        configureSearchController(searchController)
        restaurantController.fetchRestaurants()
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering() {
            return 1
        }
        
        if favorites.count > 0 {
            return 2
        }
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredRestaurants.count
        }
        
        if hasFavorites {
            if section == 0 {
                return favorites.count
            } else {
                return restaurants.count
            }
        } else {
            return restaurants.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTableViewCell.reuseIdentifier, for: indexPath) as! RestaurantTableViewCell
        let restaurant: Restaurant
        
        if isFiltering() {
            restaurant = filteredRestaurants[indexPath.row]
        } else {
            if hasFavorites {
                if indexPath.section == 0 {
                    restaurant = favorites[indexPath.row]
                } else {
                    restaurant = restaurants[indexPath.row]
                }
            } else {
                restaurant = restaurants[indexPath.row]
            }
        }
        
        cell.configure(restaurant, sortingBy: sortingOption)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let restaurant: Restaurant
        
        if isFiltering() {
            return nil
        }
        
        if hasFavorites {
            if indexPath.section == 0 {
                restaurant = favorites[indexPath.row]
            } else {
                restaurant = restaurants[indexPath.row]
            }
        } else {
            restaurant = restaurants[indexPath.row]
        }
        
        let action: UIContextualAction
        
        if restaurant.isFavorite {
            action = UIContextualAction(style: .normal, title: "Unfavorite", handler: { (action, view, success) in
                success(true)
                FavoriteController.sharedInstance.removeRestaurant(restaurant)
                self.restaurantController.fetchRestaurants(sortedBy: self.sortingOption)
            })
        } else {
            action = UIContextualAction(style: .normal, title: "Favorite", handler: { (action, view, success) in
                success(true)
                FavoriteController.sharedInstance.saveRestaurant(restaurant)
                self.restaurantController.fetchRestaurants(sortedBy: self.sortingOption)
            })
        }
        
        action.backgroundColor = UIColor.red
        
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering() {
            return nil
        }
        
        if favorites.count > 0 {
            if section == 0 {
                return "Favorites"
            } else {
                return "Restaurants"
            }
        }
        
        return nil
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let navigationController = segue.destination as! UINavigationController
        let viewController = navigationController.viewControllers.first as! SortingOptionsTableViewController
        
        viewController.delegate = self
    }
    
    
    // MARK: Private methods
    
    private func configureTableView(_ tableView: UITableView) {
        tableView.register(UINib(nibName: RestaurantTableViewCell.nibName, bundle: Bundle.main), forCellReuseIdentifier: RestaurantTableViewCell.reuseIdentifier)
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100.0
    }
    
    private func configureSearchController(_ searchController: UISearchController) {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Restaurant"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    private func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        var allRestaurants: [Restaurant] = []
        allRestaurants.append(contentsOf: restaurants)
        allRestaurants.append(contentsOf: favorites)
        
        filteredRestaurants = allRestaurants.filter({(restaurant: Restaurant) -> Bool in
            return restaurant.name.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
}


// MARK: RestaurantController delegate methods

extension RestaurantsTableViewController: RestaurantControllerDelegate {
    
    func didFetchRestaurants(_ restaurants: [Restaurant], favorites: [Restaurant]) {
        self.favorites = favorites
        self.restaurants = restaurants
    }
    
    func didFailFetchingRestaurants() {
        
    }
}


// MARK: SortingOptionsTableViewController delegate methods

extension RestaurantsTableViewController: SortingOptionsTableViewControllerDelegate {
    
    func didSelectSortOption(controller: SortingOptionsTableViewController, sortOption: Sorting) {
        sortingOption = sortOption
        navigationItem.rightBarButtonItem?.title = sortOption.description
        restaurantController.fetchRestaurants(sortedBy: sortOption)
        controller.dismiss(animated: true)
    }
    
    func didCancel(controller: SortingOptionsTableViewController) {
        controller.dismiss(animated: true)
    }
}


// MARK: UISearchResults updating methods

extension RestaurantsTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        
        filterContentForSearchText(searchText)
    }
}
