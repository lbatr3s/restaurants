//
//  SortingOptionsTableViewController.swift
//  TakeAwayRestaurants
//
//  Created by Lester Batres on 11/26/17.
//  Copyright © 2017 Lester Batres. All rights reserved.
//

import UIKit

protocol SortingOptionsTableViewControllerDelegate: class {
    func didSelectSortOption(controller: SortingOptionsTableViewController, sortOption: Sorting)
    func didCancel(controller: SortingOptionsTableViewController)
}

class SortingOptionsTableViewController: UITableViewController {
    
    private let options: [Sorting] = {
        return [.bestMatch, .newest, .ratingAverage, .distance, .popularity, .averageProductPrice, .deliveryCosts, .minCost, .none]
    }()
    
    weak var delegate: SortingOptionsTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    // MARK: Action methods
    
    @IBAction func didTapCancelButton(_ sender: UIBarButtonItem) {
        delegate?.didCancel(controller: self)
    }
    
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        let option = options[indexPath.row]
        
        cell.textLabel?.text = option.description

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedOption = options[indexPath.row]
        
        delegate?.didSelectSortOption(controller: self, sortOption: selectedOption)
    }
}
